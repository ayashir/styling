require 'rails_helper'
require 'spec_helper'
include HoursHelper

describe "Hours Testing Suite" do

  before(:each) do
    @admin = create(:user, admin: true)
  end

  describe "User Visibility" do
    context "#Admin" do

      before(:each) do
        @user = create(:user)
        @type = create(:type)
        @business = create(:business, type: @type, user: @user)
        @days = create_list(:day, 8)
        @hours = create_list(:hour, 5, business: @business, day: @days.sample)
      end

      # Enable load seed if issue with days

      it "should be shown Hours Menu" do
        sign_in @admin
        visit root_path
        expect(page).to have_content 'Hours'
      end

      it "should be shown all hours" do
        sign_in @admin
        visit root_path
        visit hours_path
        expect(page).to have_content 'Hours'
        expect(page).to have_content formatTime @hours.sample.time_start
        expect(page).to have_content formatTime @hours.sample.time_end
      end

      it "should see business name in hours#index" do
        sign_in @admin
        visit root_path
        visit hours_path
        expect(page).to have_content 'Hours'
        expect(page).to have_content @business.name
      end

    end
    context "#Regular" do
      before(:each) do

        @type = create(:type)

        @userA = create(:user)
        @businessA = create(:business, type: @type, user: @userA)

        @userB = create(:user)
        @businessB = create(:business, type: @type, user: @userB)

        @days = create_list(:day, 8)
        @hoursA = create_list(:hour, 5, business: @businessA, day: @days.sample)
        @hoursB = create_list(:hour, 5, business: @businessB, day: @days.sample, time_start: Time.now.beginning_of_day + rand(1..12).hours, time_end: Time.now.beginning_of_day + rand(12..23).hours)

      end

      it "should only be shown hours in hours#index for User's Business" do
        sign_in @userA
        visit root_path
        visit hours_path
        expect(page).to have_content 'Hours'
        expect(page).to have_content formatTime @hoursA.sample.time_start
        expect(page).to have_content formatTime @hoursA.sample.time_end

        expect(page).to have_css 'tr', count: 5 + 1
      end

      it "should not see business name hours#index" do
        sign_in @userA
        visit root_path
        visit hours_path
        expect(page).to have_content 'Hours'

        expect(page).to have_no_content '<th>Business</th>'
        expect(page).to have_no_content @businessA.name

      end

      it "should be able to access hours/id for User's Business" do
        sign_in @userA
        visit root_path
        visit hour_path @hoursA.first
        expect(page).to have_content @hoursA.first.business.name
      end

      it "should be able to access hours/id/edit for User's Business" do
        sign_in @userA
        visit root_path
        visit edit_hour_path @hoursA.first
        expect(page).to have_content 'Editing Hour'
      end

    end
  end

  describe "CRUD" do
    before(:each) do
      @user = create(:user)
      @business = create(:business, user: @user)
      @days = create_list(:day, 8)
      @hours = create_list(:hour, 5, business: @business, day: @days.sample, time_start: Time.now.beginning_of_day + rand(1..12).hours, time_end: Time.now.beginning_of_day + rand(12..23).hours)
    end
    context "#Admin" do

      it "should be able to add new Hour for any user" do
        sign_in @admin
        visit root_path
        visit new_hour_path
        expect(page).to have_content 'New Hour'

        find('#select_day select option[value="1"]').select_option
        find('#hour_time_start_4i option[value="09"]').select_option
        find('#hour_time_end_5i option[value="18"]').select_option
        find('#business select option[value="1"]').select_option

        click_button 'Submit'

        expect(page).to have_content 'Working Hours'
        expect(page).to have_content @business.name

      end

      it "should be able to update any Hour for any user" do
        sign_in @admin
        visit root_path
        visit edit_hour_path @hours.first

        find('#select_day select option[value="2"]').select_option
        find('#hour_time_start_4i option[value="10"]').select_option
        find('#hour_time_end_5i option[value="19"]').select_option

        click_button 'Submit'

        expect(page).to have_content 'Working Hours'
        expect(page).to have_content @business.name

      end

    end
    context "#Regular" do
      it "should be able to add new hour" do
        sign_in @user
        visit root_path
        visit new_hour_path
        expect(page).to have_content 'New Hour'

        find('#select_day select option[value="1"]').select_option
        find('#hour_time_start_4i option[value="09"]').select_option
        find('#hour_time_end_5i option[value="18"]').select_option

        click_button 'Submit'
        expect(page).to have_content 'Working Hours'
        expect(page).to have_content @business.name
      end
      it "should be able to update hours/id/edit" do
        sign_in @user
        visit root_path
        visit edit_hour_path @hours.first
        expect(page).to have_content 'Editing Hour'

        find('#select_day select option[value="1"]').select_option
        find('#hour_time_start_4i option[value="08"]').select_option
        find('#hour_time_end_5i option[value="16"]').select_option

        click_button 'Submit'
        expect(page).to have_content 'Working Hours'
        expect(page).to have_content @business.name

      end
    end
  end

end
