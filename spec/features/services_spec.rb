require 'rails_helper'
require 'spec_helper'

describe "Services Testing Suite" do

  describe "User Visibility" do
    context "#Admin" do

      before(:each) do
        @admin = create(:user, admin: true)
        @user = create(:user)
        @business = create(:business, user: @user)
        @services = create_list(:service, 5, business: @business)
      end

      it "should be shown Services Menu" do
        sign_in @admin
        visit root_path
        expect(page).to have_content 'Services'
      end

      it "should be shown all services" do
        sign_in @admin
        visit root_path
        visit services_path
        expect(page).to have_content 'Services'
        expect(page).to have_content @services.first.name
        expect(page).to have_content @services.last.name
      end
      it "should see business name in services#index" do
        sign_in @admin
        visit root_path
        visit services_path
        expect(page).to have_content 'Business'
        expect(page).to have_content @business.name
      end
    end

    context "#Regular" do
      before(:each) do
        @userA = create(:user)
        @businessA = create(:business, user: @userA)
        @servicesA = create_list(:service, 5, business: @businessA)

        @userB = create(:user)
        @businessB = create(:business, user: @userB)
        @servicesB = create_list(:service, 5, business: @businessB)
      end

      it "should only be shown services in services#index for User's Business" do
        sign_in @userA
        visit root_path
        visit services_path
        expect(page).to have_content 'Services'
        expect(page).to have_content @servicesA.first.name
        expect(page).to have_content @servicesA.last.name

        expect(page).to have_no_content @servicesB.first.name
        expect(page).to have_no_content @servicesB.last.name
        sign_out @userA
      end
      it "should not see business name services#index" do
        sign_in @userA
        visit root_path
        visit services_path
        expect(page).to have_content 'Services'

        expect(page).to have_no_content '<th>Business</th>'
        expect(page).to have_no_content @businessA.name
      end
      it "should be able to access services/id for User's Business" do
        sign_in @userA
        visit root_path
        visit service_path @servicesA.first
        expect(page).to have_content 'Service'
        expect(page).to have_content @businessA.name
      end
    end
  end

  describe "CRUD" do
    context "#Admin" do
      before(:each) do
        @admin = create(:user, admin: true)
        @userA = create(:user)
        @businessA = create(:business, user: @userA)
        @service = create(:service, business: @businessA)
        @updateService = build(:service, business: @businessA)
      end
      it "should be able to add new Services for any user" do
        sign_in @admin
        visit root_path
        visit new_service_path
        fill_in 'service[name]', with: @service.name
        fill_in 'service[description]', with: @service.description
        fill_in 'service[price]', with: @service.price
        click_button('Submit')
        expect(page).to have_content 'Service'
        expect(page).to have_content @service.name
        expect(page).to have_content @service.business.name
      end
      it "should be able to update any Service for any user" do
        sign_in @admin
        visit root_path
        visit edit_service_path @service
        fill_in 'service[name]', with: @updateService.name
        fill_in 'service[description]', with: @updateService.description
        fill_in 'service[price]', with: @updateService.price
        click_button('Submit')
        expect(page).to have_content 'Service'
        expect(page).to have_content @updateService.name
        expect(page).to have_content @updateService.business.name
      end
      pending "How to delete?"
      it "should be able to delete any Service for any user" do
      end
    end
    context "#Regular" do
      before(:each) do
        @user = create(:user)
        @business = create(:business, user: @user)
        @service = build(:service, business: @business)
        @service_created = create(:service, business: @business)
        @service_updated = build(:service, business: @business)
      end
      it "should be able to add new service" do
        sign_in @user
        visit root_path
        visit new_service_path
        fill_in 'service[name]', with: @service.name
        fill_in 'service[description]', with: @service.description
        fill_in 'service[price]', with: @service.price
        click_button('Submit')
        expect(page).to have_content @service.name
        expect(page).to have_content @service.description
      end
      it "should be able to update services/id/edit for User's Business" do
        sign_in @user
        visit root_path
        visit edit_service_path @service_created
        fill_in 'service[name]', with: @service_updated.name
        fill_in 'service[description]', with: @service_updated.description
        fill_in 'service[price]', with: @service_updated.price
        click_button('Submit')
        expect(page).to have_content @service_updated.name
        expect(page).to have_content @service_updated.description
      end
    end
  end

end
