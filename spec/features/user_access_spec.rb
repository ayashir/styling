require 'rails_helper'
require 'spec_helper'

# feature "User Visibility" do
#   scenario "Stranger visits app" do
#     visit root_path
#     page.should have_no_content('Services')
#     page.should have_no_content('Hours')
#   end
# end

describe "User Visibility" do

  describe "Stranger Usage" do
    context "#Visitor business show" do
      let(:type) { create(:type) }
      let(:business) { create(:business, type: type) }
      it "should be able to visit /show/ page without logging" do
        visit root_path
        visit business_path business
        expect(page).to have_current_path business_path business
      end
    end
    context "CRUD Operation for Stranger will redirect to login page" do
      it "should redirect to login page if Stranger acess business/new page" do
        visit root_path
        visit new_business_path
        expect(page).to have_current_path new_user_session_path
      end
    end
    it "should not display Services and Hours but Businesses and For Professionals" do
      visit root_path
      page.should have_no_content('Services')
      page.should have_no_content('Hours')

      page.should have_content('Businesses')
      page.should have_content('For Professionals')
    end
    it "should redirect to root path if Stranger is trying to access services and hours path" do
      visit root_path
      visit "services#index"
      expect(page).to have_current_path(new_user_session_path)

      visit root_path
      visit "hours#index"
      expect(page).to have_current_path(new_user_session_path)
    end
  end

  describe "Registering & Logging Usage" do

    describe "Register User" do

      let(:user) { build(:user) }

      it "should register Stranger to Registered User " do
        user
        visit root_path
        visit new_user_registration_path
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        fill_in 'user[password_confirmation]', with: user.password_confirmation
        find('.sign-up-button').click
        expect(page).to have_current_path(root_path)
      end


    end

    describe "Log In User" do

      let(:user) { create(:user) }

      it "should log a Stranger to Register User" do
        user
        visit root_path
        visit new_user_session_path
        fill_in 'user[email]', with: user.email
        fill_in 'user[password]', with: user.password
        find('.log-in-button').click
        expect(page).to have_current_path(root_path)
      end

      # test for displaying login email
      it "should display the email of the logged in user" do
        sign_in user
        visit root_path
        expect(page).to have_content user.email
      end
    end

    describe "Log Out User" do
      let(:user) { create(:user) }
      it "should enable the user to successfully log out" do
        visit root_path
        sign_in user
        visit root_path
        find('.sign-out-button').click
        expect(page).to have_content('For Professionals')
      end

      # test for removing login email
      it "should not display the email of the logged in user" do
        sign_in user
        visit root_path
        expect(page).to have_content user.email
        sign_out user
        visit root_path
        expect(page).to have_no_content user.email
      end
    end

  end

  describe "User Business Management" do

    describe "Registered User" do

      describe "Adding a New Business" do
        describe "User can only create a new Business if he/she has not created yet" do

          let(:user) { create(:user) }

          it "should display New Business when user has no business created" do
            sign_in user
            visit root_path
            expect(page).to have_content('New Business')
          end

          it "should show business/new when user has no business created" do
            sign_in user
            visit root_path
            visit new_business_path
            expect(page).to have_content 'New Business'
          end
        end

        describe "User cannot see Services & Hours when he/she has not created a business yet" do
          let(:user) { create(:user) }
          let(:business) { create(:business) }

          it "should not display Services & Hours in the nav menu" do
            sign_in user
            visit root_path

            expect(page).to have_no_content('Services')
            expect(page).to have_no_content('Hours')
          end
        end
      end

      describe "Register User can only edit his/her business" do
        let(:user) { create(:user) }
        let(:business) { create(:business, :user => user) }
        it "should allow businesses/id/edit" do
          sign_in user
          visit root_path
          visit edit_business_path(business)
          expect(page).to have_content 'Editing Business'
        end

        let(:userA) { create(:user) }
        let(:businessA) { create(:business, :user => userA) }

        let(:userB) { create(:user) }
        let(:businessB) { create(:business, :user => userB) }

        it "should not enable User A to see businesses/id/edit for User B" do
          sign_in userA
          visit edit_business_path businessB
          expect(page).to have_current_path root_path
          sign_out userA

          sign_in userB
          visit edit_business_path businessA
          expect(page).to have_current_path root_path
        end

      end

      describe "Register User cannot see Active Checkbox in new or edit business" do

        let(:user) { create(:user) }

        it "should not display Active Checkbox in business/new" do
          sign_in user
          visit root_path
          visit new_business_path
          expect(page).to have_content 'Name'
          expect(page).to have_no_content 'Active'
        end

        let(:business) { create(:business, user: user) }

        it "should not display Active Checkbox in business/id/edit" do
          sign_in user
          visit root_path
          visit edit_business_path business
          expect(page).to have_content 'Name'
          expect(page).to have_no_content 'Active'
        end
      end

      describe "Registered User cannot see User select box in business _form" do
        let(:user) { create(:user) }
        it "should not display user select box for new business" do
          sign_in user
          visit root_path
          visit new_business_path
          expect(page).to have_no_content 'User'
        end

        let(:business) { create(:business, user: user) }
        it "should not display user select box for edit business" do
          sign_in user
          visit root_path
          visit edit_business_path business
          expect(page).to have_no_content 'User'
        end
      end
    end

    describe "Admin User" do

      describe "Admin User can edit all Businesses" do

        let(:userA) { create(:user) }
        let(:businessA) { create(:business).reload }

        let(:userB) { create(:user) }
        let(:businessB) { create(:business).reload }

        let(:userC) { create(:user) }
        let(:businessC) { create(:business).reload }

        let(:userAdmin) { create(:user, :admin) }

        it "should enable the Admin User to view the business/edit page for any business" do
          sign_in userAdmin
          visit root_path
          visit edit_business_path(businessA)
          expect(page).to have_content 'Editing Business'

          visit root_path
          visit edit_business_path(businessB)
          expect(page).to have_content 'Editing Business'

          visit root_path
          visit edit_business_path(businessC)
          expect(page).to have_content 'Editing Business'

        end

      end

      describe "Admin User can update the business user" do

        let(:userAdmin) { create(:user, :admin) }
        it "should display a select box displaying the list of users in the system" do
          sign_in userAdmin
          visit root_path
          visit new_business_path
          expect(page).to have_content 'User'
        end
      end

      describe "Admin User can add new Business" do
        # create normal user
        let(:type) { create(:type) }
        let(:user) { create(:user) }
        # create admin
        let(:admin) { create(:user, admin: true) }
        let(:business) { build(:business, user: user, type: type) }

        # create business ??
        it "should display show page for new business" do

          pending "Cannot select type from capybara"
          sign_in admin

          visit root_path
          visit new_business_path

          fill_in 'business[name]', with: business.name
          fill_in 'business[address1]', with: business.address1

          #puts page.body
          #puts type.inspect
          #puts business.inspect
          #puts find_by_id('business_type').find('.type')
          #find(:xpath, '//*[@id="business_type"]/select/option')

          click_button('Submit')

          expect(page).to have_content 'Details'

          # select user, from: 'business[user_id]'
          # select normal user
          # click on submit
          # expect show page to happen

        end
      end

      describe "Only Admin User can see Active checkbox in business/edit" do
        let(:user) { create(:user) }
        let(:business) { create(:business).reload }

        let(:userAdmin) { create(:user, :admin) }
        it "should display Active checkbox for the admin" do
          sign_in userAdmin
          visit root_path
          visit edit_business_path business
          expect(page).to have_content 'Active'
        end
      end

    end


  end

  describe "when User puts a non matched url that does not exist" do
    it "should be redirected to the root page" do
      visit root_path
      visit '/users'
      expect(page).to have_current_path root_path
    end
  end

end
