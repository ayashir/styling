FactoryBot.define do

  factory :business do

    sequence(:name) { |n| "Salon #{n}" }
    sequence(:email) { |n| "salon#{n}@mail.com" }
    address1 "Address"
    active true
    association :type, factory: :type
    association :user, factory: :user

    #factory :business_with_user do
      #after(:create) do |business|
        #create(:type, type: type)
        #create(:user, user: user)
      #end
    #end

  end

end
