FactoryBot.define do

  factory :photo do

    sequence(:name) { |n| "image#{n}" }
    sequence(:caption) { |n| "image_caption#{n}" }
    active true
    image { Rack::Test::UploadedFile.new(Rails.root.join('spec', 'fixtures', 'photo.jpg'), 'image/jpeg') }
    association :business, factory: :business

  end

end
