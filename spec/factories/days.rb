FactoryBot.define do

  days = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
    "Holiday"
  ]

  factory :day do
    sequence(:name) { |n| "#{days[n]}" }
  end

end
