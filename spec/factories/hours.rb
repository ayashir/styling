FactoryBot.define do

  factory :hour do

    active true
    time_start { Time.now.beginning_of_day + 9.0.hours }
    time_end { Time.now.beginning_of_day + 9.0.hours + 9.0.hours }
    association :day, factory: :day
    association :business, factory: :business

    trait :time_start do
      time_start { Time.now.beginning_of_day + rand(1..9).hours }
    end

    trait :time_end do
      time_end { Time.now.beginning_of_day + rand(1..9).hours }
    end

  end

end
