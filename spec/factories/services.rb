FactoryBot.define do

  factory :service do

    sequence(:name) { |n| "Hair Styling Service #{n}" }
    sequence(:description) { |n| "Hair Styling Service Description #{n}" }
    price 150
    association :business, factory: :business

  end

end