FactoryBot.define do

  factory :user do
    transient do
      skip_confirmation true
    end

    sequence(:email) { |n| "test#{n}@test.com" }
    password "123"
    password_confirmation "123"
    admin false

    trait :admin do
      admin true
    end

  end

end
