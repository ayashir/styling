FactoryBot.define do

  factory :type do
    name "Salon Type Name"
    description "Salon Type Desc"
  end

end
