class Business < ApplicationRecord
  belongs_to :type
  belongs_to :user, optional: true
  validates :name, :address1, presence: true
  has_many :service
  has_many :photo

  before_save :default_values

  def default_values
    if self.active.nil?
      self.active = true
    elsif self.active == 1
      self.active = true
    elsif self.active == 0
      self.active = false
    end
  end

end
