class Service < ApplicationRecord
  belongs_to :business
  validates :name, :business, presence:true
end
