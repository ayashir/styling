class HoursController < ApplicationController
  before_action :set_hour, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  include BusinessesHelper
  include HoursHelper
  before_action :redirect_root_if_no_business!

  # GET /hours
  # GET /hours.json
  def index
    if is_current_user_admin?
      @hours = Hour.all.includes(:business).references(:business)
    else
      @hours = Hour.where(business_id: get_user_business.first.id)
    end
  end

  # GET /hours/1
  # GET /hours/1.json
  def show
  end

  # GET /hours/new
  def new
    @hour = Hour.new
    @hour.time_start = DateTime.now.midnight
    @hour.time_end = DateTime.now.midnight
  end

  # GET /hours/1/edit
  def edit
  end

  # POST /hours
  # POST /hours.json
  def create
    @hour = Hour.new(hour_params)
    if !is_current_user_admin?
      @hour.business_id = get_user_business.first.id
    end

    respond_to do |format|
      if @hour.save
        format.html { redirect_to @hour, notice: 'Hour was successfully created.' }
        format.json { render :show, status: :created, location: @hour }
      else
        format.html { render :new }
        format.json { render json: @hour.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hours/1
  # PATCH/PUT /hours/1.json
  def update
    respond_to do |format|
      if @hour.update(hour_params)
        format.html { redirect_to @hour, notice: 'Hour was successfully updated.' }
        format.json { render :show, status: :ok, location: @hour }
      else
        format.html { render :edit }
        format.json { render json: @hour.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hours/1
  # DELETE /hours/1.json
  def destroy
    @hour.destroy
    respond_to do |format|
      format.html { redirect_to hours_url, notice: 'Hour was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hour
      begin
        @hour = Hour.find(params[:id])
      rescue
        redirect_to businesses_url
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hour_params
      params.require(:hour).permit(:day_id, :active, :time_start, :time_end, :business_id)
    end
end
