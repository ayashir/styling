class BusinessesController < ApplicationController
  before_action :set_business, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :edit, :update, :destroy]
  include BusinessesHelper

  # GET /businesses
  # GET /businesses.json
  def index
    @businesses = Business.where(active: true).distinct.left_joins(:photo).includes(:photo).order(:name)
    @paginate_business = Kaminari.paginate_array(@businesses).page(params[:page])
  end

  # GET /businesses/1
  # GET /businesses/1.json
  def show
    begin
      @business = Business.find(params[:id])
      @services = Service.where(:business => @business)
      @hours = Hour.where('business_id': @business).distinct
      @image = ''
      photo = Photo.where(:business => @business).limit(1)
      if photo.any?
        @image = photo.first.image
      end
    rescue
      redirect_to businesses_url
    end
  end

  # GET /businesses/new
  def new
    redirect_business
    @business = Business.new
  end

  # GET /businesses/1/edit
  def edit
    @business = Business.find(params[:id])
    #redirect_if_not_user_business! @business
    # write minimum code to make test pass
    # to refactor with helper method if possible
    if user_signed_in? && !current_user.admin && !@business.user.eql?(current_user)
      redirect_to root_path
    end
  end

  # POST /businesses
  # POST /businesses.json
  def create
    @business = Business.new(business_params)
    @business.user = current_user

    respond_to do |format|
      if @business.save
        format.html { redirect_to @business, notice: 'Business was successfully created.' }
        format.json { render :show, status: :created, location: @business }
      else
        format.html { render :new }
        format.json { render json: @business.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /businesses/1
  # PATCH/PUT /businesses/1.json
  def update
    respond_to do |format|
      if @business.update(business_params)
        format.html { redirect_to @business, notice: 'Business was successfully updated.' }
        format.json { render :show, status: :ok, location: @business }
      else
        format.html { render :edit }
        format.json { render json: @business.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /businesses/1
  # DELETE /businesses/1.json
  def destroy
    @business.destroy
    respond_to do |format|
      format.html { redirect_to businesses_url, notice: 'Business was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_business
      begin
        @business = Business.find(params[:id])
      rescue
        redirect_to businesses_url
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def business_params
      params.require(:business).permit(:name, :description, :active, :address1, :address2, :zipcode, :phone_no, :mobile_no, :email, :fax, :type_id)
    end
end
