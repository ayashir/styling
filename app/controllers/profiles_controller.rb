class ProfilesController < ApplicationController

  def index
  end

  def show
    @business = Business.find(params[:id])
    @photos = Photo.all.where(:business => @business)
  end

end
