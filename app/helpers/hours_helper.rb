module HoursHelper
  
  def formatDate date
    date.strftime("%B %d %Y %H %M %S")
  end

  def formatTime time
    time.strftime("%H %M")
  end

end
