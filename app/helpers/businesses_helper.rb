module BusinessesHelper

  def get_user_business
    Business.where(user_id: current_user)
  end

  def is_user_business?(business)
    if user_signed_in?
      business == get_user_business.first
    else
      false
    end
  end

  def user_has_business?
    !get_user_business.first.nil?
  end

  def redirect_business
    if !current_user.admin and user_has_business?
      redirect_to businesses_url
    end
  end

  def businesses_exist?
    Business.count > 0
  end


  def is_current_user_admin?
    if user_signed_in?
      if current_user.admin.nil?
        false
      else
        current_user.admin
      end
    end
  end

  def is_business_active?(business)
    Business.active
  end

  def redirect_if_business_not_active(business)
    if !is_business_active
      redirect_business
    end
  end

  def redirect_root_if_no_business!
    if user_signed_in? && !is_current_user_admin? && !user_has_business?
      redirect_to root_path
    end
  end

  def redirect_if_not_user_business!(business)
    if user_signed_in? && !current_user.admin && !business.user.eql?(current_user)
      redirect_to root_path
    end
  end

  def redirect_if_not_admin!
    if !is_current_user_admin?
      redirect_to root_path
    end
  end

end
