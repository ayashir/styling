json.extract! photo, :id, :name, :caption, :active, :image, :business_id, :created_at, :updated_at
json.url photo_url(photo, format: :json)
