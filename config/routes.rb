Rails.application.routes.draw do
  resources :photos
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'businesses#index'
  resources :businesses
  resources :services
  resources :hours
  resources :types
  resources :days
  resources :profiles, only: [:show]
  # get '/profile/:id', to: 'profiles#show'
  get '*path' => redirect('/')
end
